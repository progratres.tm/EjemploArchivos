import java.io.Serializable;
import java.util.ArrayList;

public class LasPersonas implements Serializable
{
	private static final long serialVersionUID = 1L;
	private ArrayList<Persona> lasPersonas;
	
	public LasPersonas()
	{
		lasPersonas = new ArrayList<Persona>();
	}
	
	public void agregarPersona(Persona p)
	{
		lasPersonas.add(p);
	}
	
	public Persona iesima(int i)
	{
		return lasPersonas.get(i);
	}
	
	public int cantidad()
	{
		return lasPersonas.size();
	}
}
