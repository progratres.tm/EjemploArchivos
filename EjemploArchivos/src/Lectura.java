import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Lectura 
{
	public static void main(String[] args) 
	{
		// Archivo del que queremos leer
		// (debemos especificar la ruta -> ejem: c:\\nombreCarpeta\...\fichero.txt o \var\www\fichero.txt)
		File archivo = new File("leer.txt");
		
		lectura1(archivo);
		lectura2(archivo);
	}

	private static void lectura1(File archivo) 
	{
			try {
				FileInputStream fis = new FileInputStream(archivo);
				Scanner scanner = new Scanner(fis);
				System.out.println("Metodo 1 de lectura");
				while (scanner.hasNextLine()) 
				{
					String linea = scanner.nextLine(); 	
					System.out.println(linea);		
				}
				scanner.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}

	private static void lectura2(File archivo) 
	{
		Scanner s = null;

		try {
			System.out.println("... Leemos el contenido del archivo ...");
			s = new Scanner(archivo);

			while (s.hasNextLine()) {
				String linea = s.nextLine(); 	
				System.out.println(linea);		
			}
			s.close();

		} catch (Exception ex) {
			System.out.println("Mensaje: " + ex.getMessage());
		}
	}
}