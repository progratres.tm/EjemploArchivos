import java.io.Serializable;

public class Persona implements Serializable
{
	private static final long serialVersionUID = 1L;
	private String _nombre;
	private int _edad;
	
	public Persona(String _nombre, int _edad) 
	{
		this._nombre = _nombre;
		this._edad = _edad;
	}

	public String nombre() 
	{
		return _nombre;
	}

	public int edad() 
	{
		return _edad;
	}
}
