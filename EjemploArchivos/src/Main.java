import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Main 
{
	public static void main(String[] args) 
	{
		Persona p1 = new Persona("Ana", 13);
		Persona p2 = new Persona("Maria", 15);
		Persona p3 = new Persona("Juan", 17);
		
		LasPersonas todas = new LasPersonas();
		todas.agregarPersona(p1);
		todas.agregarPersona(p2);
		todas.agregarPersona(p3);
		
		ObjectOutputStream out;
		try {
			out = new ObjectOutputStream(new FileOutputStream("personas.ser"));
			out.writeObject(p1);
			out.writeObject(p2);
			out.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		serializarObjeto("estructura.ser",todas);
		
		LasPersonas deDisco = (LasPersonas) deserializarObjeto("estructura.ser");
		System.out.println("Las personas leidas de disco");
		
		for (int i=0; i<deDisco.cantidad();++i)
			System.out.println("Persona: " + deDisco.iesima(i).nombre());
	}

	private static void serializarObjeto(String arch, Object todas) 
	{
		ObjectOutputStream estructura;
		try {
			estructura = new ObjectOutputStream(new FileOutputStream(new File(arch)));
			estructura.writeObject(todas);
			estructura.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private static Object deserializarObjeto(String arch)
	{
		ObjectInputStream leido;
		Object ret = null;
		try {
			leido = new ObjectInputStream(new FileInputStream(new File(arch)));
			ret = leido.readObject();
			leido.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return ret;	
	}
}
